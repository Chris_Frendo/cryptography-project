#### Creating Self-Signed Certificate

1. Generate the keystore using keytool and when asked for "First and Last name" specify: localhost
   ```
   keytool -genkeypair -alias localhost -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore <filename>.p12 -validity 3650 
       -ext SAN=dns:localhost -keypass <password> -storepass <password>
   ```
2. Export to .crt
    ```
    keytool -export -keystore <filename>.p12 -alias localhost -file <filename>.crt
    ```
3. Import the .crt file into Google Chrome root authorities
4. Import the .p12 file into Firefox in "Your Certificates"
5. Add server exception in firefox
6. Set the following option in about:config to true "security.enterprise_roots.enabled"

#### Running the server

```
java -jar SoftwareSecurityModule.jar "q&nbeP+LDeZu6=7cAvPNEk_%" "z@5+4$%G-Jb&XZgrN=8WT$_N"
```