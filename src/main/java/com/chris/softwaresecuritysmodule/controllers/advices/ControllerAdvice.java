package com.chris.softwaresecuritysmodule.controllers.advices;

import com.chris.softwaresecuritysmodule.exceptions.AdminNotValidatedException;
import com.chris.softwaresecuritysmodule.exceptions.AutoRotateDisabledException;
import com.chris.softwaresecuritysmodule.exceptions.DestroyedKeyException;
import com.chris.softwaresecuritysmodule.exceptions.EntityNotFoundException;
import com.chris.softwaresecuritysmodule.exceptions.InternalServerException;
import com.chris.softwaresecuritysmodule.exceptions.InvalidAlgorithmException;
import com.chris.softwaresecuritysmodule.exceptions.MKPartsNotValidException;
import com.chris.softwaresecuritysmodule.exceptions.MacNotVerifiedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@org.springframework.web.bind.annotation.ControllerAdvice
@RequestMapping(produces = "application/vnd.error+json")
public class ControllerAdvice extends ResponseEntityExceptionHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ControllerAdvice.class);
    private static final String ERROR_MESSAGE_PREFIX = "Failed Request {}";

    @ExceptionHandler(AdminNotValidatedException.class)
    public ResponseEntity<Object> adminNotValidatedException(AdminNotValidatedException e, WebRequest request) {
        LOGGER.warn(ERROR_MESSAGE_PREFIX, request.getDescription(false), e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(InternalServerException.class)
    public ResponseEntity<Object> internalServerException(InternalServerException e, WebRequest request) {
        LOGGER.warn(ERROR_MESSAGE_PREFIX, request.getDescription(false), e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MacNotVerifiedException.class)
    public ResponseEntity<Object> macNotVerifiedException(MacNotVerifiedException e, WebRequest request) {
        LOGGER.warn(ERROR_MESSAGE_PREFIX, request.getDescription(false), e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> entityNotFoundException(EntityNotFoundException e, WebRequest request) {
        LOGGER.warn(ERROR_MESSAGE_PREFIX, request.getDescription(false), e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidAlgorithmException.class)
    public ResponseEntity<Object> invalidAlgorithmException(InvalidAlgorithmException e, WebRequest request) {
        LOGGER.warn(ERROR_MESSAGE_PREFIX, request.getDescription(false), e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DestroyedKeyException.class)
    public ResponseEntity<Object> destroyedKeyException(DestroyedKeyException e, WebRequest request) {
        LOGGER.warn(ERROR_MESSAGE_PREFIX, request.getDescription(false), e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(AutoRotateDisabledException.class)
    public ResponseEntity<Object> autoRotateDisabledException(AutoRotateDisabledException e, WebRequest request) {
        LOGGER.warn(ERROR_MESSAGE_PREFIX, request.getDescription(false), e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MKPartsNotValidException.class)
    public ResponseEntity<Object> MKPartsNotValidException(MKPartsNotValidException e, WebRequest request) {
        LOGGER.warn(ERROR_MESSAGE_PREFIX, request.getDescription(false), e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}