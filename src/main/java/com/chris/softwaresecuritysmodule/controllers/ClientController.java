package com.chris.softwaresecuritysmodule.controllers;

import com.chris.softwaresecuritysmodule.entities.entitybodies.DecryptRequestBody;
import com.chris.softwaresecuritysmodule.entities.entitybodies.EncryptSignRequestBody;
import com.chris.softwaresecuritysmodule.entities.entitybodies.EncryptSignRequestResponseBody;
import com.chris.softwaresecuritysmodule.entities.entitybodies.VerifyIntegrityRequestBody;
import com.chris.softwaresecuritysmodule.services.ClientService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@PreAuthorize("hasRole('ROLE_CLIENT')")
@Controller
public class ClientController {

    private static Logger LOGGER = LoggerFactory.getLogger(ClientController.class);

    private final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("/encrypt")
    public String encryptForm(Model model) {
        model.addAttribute("encrypt", new EncryptSignRequestBody());
        return "encrypt";
    }

    @PostMapping(value = "/encrypt", headers = "Accept=application/json", produces = "application/json")
    public String encryptMessage(@ModelAttribute @RequestBody EncryptSignRequestBody encryptSignRequestBody, Model model) {
        LOGGER.info("Received request to encrypt a message using key with name: [{}]", encryptSignRequestBody.getApplicationKeyName());
        EncryptSignRequestResponseBody encryptResponseBody = clientService.encryptSignMessageHandler(encryptSignRequestBody, true);
        model.addAttribute("encryptResponse", encryptResponseBody);
        return "encrypt-response";
    }

    @GetMapping("/decrypt")
    public String decryptForm(Model model) {
        model.addAttribute("decrypt", new DecryptRequestBody());
        return "decrypt";
    }

    @PostMapping(value = "/decrypt", headers = "Accept=application/json", produces = "application/json")
    public String decryptMessage(@ModelAttribute @RequestBody DecryptRequestBody decryptRequestBody, Model model) {
        LOGGER.info("Received request to decrypt a message using key with name: [{}] and version: [{}]", decryptRequestBody.getApplicationKeyName(), decryptRequestBody.getKeyVersion());
        String response = clientService.decryptVerifyHandler(decryptRequestBody.getApplicationKeyName(), decryptRequestBody.getKeyVersion(), decryptRequestBody.getCipherText(), null, true);
        model.addAttribute("decryptedMessage", response);
        return "decrypt-response";
    }

    @GetMapping("/sign")
    public String signForm(Model model) {
        model.addAttribute("sign", new EncryptSignRequestBody());
        return "sign";
    }

    @PostMapping(value = "/sign", headers = "Accept=application/json", produces = "application/json")
    public String signMessage(@ModelAttribute @RequestBody EncryptSignRequestBody signRequestBody, Model model) {
        LOGGER.info("Received request to sign a message using key with name: [{}]", signRequestBody.getApplicationKeyName());
        EncryptSignRequestResponseBody signRequestResponse = clientService.encryptSignMessageHandler(signRequestBody, false);
        model.addAttribute("signResponse", signRequestResponse);
        return "sign-response";
    }

    @GetMapping("/verify")
    public String verifyForm(Model model) {
        model.addAttribute("verify", new VerifyIntegrityRequestBody());
        return "verify";
    }

    @PostMapping(value = "/verify", headers = "Accept=application/json", produces = "application/json")
    public String verifyMessage(@ModelAttribute @RequestBody VerifyIntegrityRequestBody verifyIntegrityRequestBody, Model model) {
        LOGGER.info("Received request to verify a message digest using key with name: [{}]", verifyIntegrityRequestBody.getApplicationKeyName());
        String response = clientService.decryptVerifyHandler(verifyIntegrityRequestBody.getApplicationKeyName(), verifyIntegrityRequestBody.getKeyVersion(), verifyIntegrityRequestBody.getMessage(), verifyIntegrityRequestBody.getDigest(), false);
        model.addAttribute("verifyResult", response);
        return "verify-response";
    }
}
