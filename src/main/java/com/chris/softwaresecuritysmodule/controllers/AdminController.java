package com.chris.softwaresecuritysmodule.controllers;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import com.chris.softwaresecuritysmodule.entities.entitybodies.AdminAuthorization;
import com.chris.softwaresecuritysmodule.entities.entitybodies.ApplicationKeyInfoResponse;
import com.chris.softwaresecuritysmodule.entities.entitybodies.ApplicationKeyRequestBody;
import com.chris.softwaresecuritysmodule.entities.entitybodies.ApplicationKeyResponseBody;
import com.chris.softwaresecuritysmodule.entities.entitybodies.ChangeMKRequestBody;
import com.chris.softwaresecuritysmodule.entities.entitybodies.UpdateKeyStateRequestBody;
import com.chris.softwaresecuritysmodule.services.AdminService;

import org.apache.commons.codec.DecoderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequestMapping("/admin")
public class AdminController {

    private static Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

    private final AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @PostMapping(value = "/application-key", headers = "Accept=application/json", produces = "application/json")
    public ResponseEntity<ApplicationKeyResponseBody> createApplicationKey(@RequestBody ApplicationKeyRequestBody applicationKeyRequestBody) {
        LOGGER.info("Received request to create a new application key");
        return new ResponseEntity<>(adminService.createApplicationKey(applicationKeyRequestBody), HttpStatus.CREATED);
    }

    @GetMapping(value = "/application-keys", headers = "Accept=application/json", produces = "application/json")
    public ResponseEntity<List<ApplicationKeyResponseBody>> getAllApplicationKeys(@RequestBody AdminAuthorization adminAuthorization) {
        LOGGER.info("Received request to get summary info on all application keys in vault");
        return new ResponseEntity<>(adminService.getAllApplicationKeys(adminAuthorization), HttpStatus.OK);
    }

    @GetMapping(value = "/application-key/{id}", headers = "Accept=application/json", produces = "application/json")
    public ResponseEntity<ApplicationKeyInfoResponse> getApplicationKeyById(@PathVariable long id, @RequestBody AdminAuthorization adminAuthorization) {
        LOGGER.info("Received request to get application key with id [{}]", id);
        return new ResponseEntity<>(adminService.getApplicationKeyById(id, adminAuthorization), HttpStatus.OK);
    }

    @PutMapping(value = "/application-key/{id}", headers = "Accept=application/json", produces = "application/json")
    public ResponseEntity<ApplicationKeyResponseBody> updateApplicationKey(@PathVariable long id, @RequestBody ApplicationKeyRequestBody applicationKeyRequestBody) {
        LOGGER.info("Received request to update application key with id [{}]", id);
        return new ResponseEntity<>(adminService.updateApplicationKey(id, applicationKeyRequestBody), HttpStatus.OK);
    }

    @PutMapping(value = "/application-key-state/{id}", headers = "Accept=application/json", produces = "application/json")
    public ResponseEntity<ApplicationKeyInfoResponse> updateApplicationKeyState(@PathVariable long id, @RequestBody UpdateKeyStateRequestBody updateKeyStateRequestBody) {
        LOGGER.info("Received request to update application key state with id [{}]", id);
        return new ResponseEntity<>(adminService.updateApplicationKeyState(id, updateKeyStateRequestBody), HttpStatus.OK);
    }

    @PostMapping(value = "/key-encryption-key-rotate", headers = "Accept=application/json", produces = "application/json")
    public ResponseEntity rotateKeyEncryptionKey(@RequestBody AdminAuthorization adminAuthorization) throws BadPaddingException, InvalidKeyException, DecoderException, IllegalBlockSizeException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException {
        LOGGER.info("Received request to rotate KEK");
        adminService.rotateKeyEncryptionKey(adminAuthorization);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(value = "/change-mk", headers = "Accept=application/json", produces = "application/json")
    public ResponseEntity changeMasterKey(@RequestBody ChangeMKRequestBody changeMKRequestBody) throws BadPaddingException, InvalidKeyException, DecoderException, IllegalBlockSizeException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException {
        LOGGER.info("Received request to change MK");
        adminService.changeMasterKey(changeMKRequestBody);
        return new ResponseEntity(HttpStatus.OK);
    }
}
