package com.chris.softwaresecuritysmodule.utils;

public enum KeyAlgorithms {
    AES("AES"),
    DES("DES"),
    TDES("DESede");

    private String value;

    KeyAlgorithms(String envUrl) {
        this.value = envUrl;
    }

    public String getValue() {
        return value;
    }

}
