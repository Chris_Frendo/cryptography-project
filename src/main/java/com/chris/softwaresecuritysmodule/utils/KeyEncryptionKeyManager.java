package com.chris.softwaresecuritysmodule.utils;

/**
 * Static Class used to store the parts of the KEK
 */
public final class KeyEncryptionKeyManager {

    private static String kek;

    public static String getKek() {
        return kek;
    }

    /**
     * Stores kek
     *
     * @param _kek The original kek
     */
    public static void setKek(String _kek) {
        kek = _kek;
    }

}
