package com.chris.softwaresecuritysmodule.utils;

/**
 * Static Class used to temporarily store the parts of the Master Key upon startup of the server
 */
public final class MasterKeyManager {

    private static String partA;
    private static String partB;

    /**
     * Removes references to the MK parts
     */
    public static void destroyKeys() {
        partA = null;
        partB = null;
    }

    /**
     * Populates mk1 and mk2
     *
     * @param _mk1 Value for mk1
     * @param _mk2 Value for mk2
     */
    public static void setKeys(String _mk1, String _mk2) {
        partA = _mk1;
        partB = _mk2;
    }

    public static String getPartA() {
        return partA;
    }

    public static String getPartB() {
        return partB;
    }
}
