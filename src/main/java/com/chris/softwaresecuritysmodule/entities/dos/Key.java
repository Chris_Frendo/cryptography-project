package com.chris.softwaresecuritysmodule.entities.dos;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import com.chris.softwaresecuritysmodule.entities.enums.KeyState;
import com.chris.softwaresecuritysmodule.exceptions.InternalServerException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

@Entity
public class Key {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "application_key_id")
    private ApplicationKey applicationKey;

    @Column(nullable = false)
    private String version;

    @Column(length = 5000)
    private String value;

    @Column(nullable = false)
    private String creationTime;

    @Column()
    private String expiryTime;

    @Column(nullable = false)
    private KeyState keyState;

    @Column(nullable = false)
    private String mac;

    @Column()
    private String iv;

    @Column(length = 5000)
    private String value2;

    public Key() {
        // jpa only
    }

    public Key(ApplicationKey applicationKey, String version, String value, String value2, String creationTime, String expiryTime, KeyState keyState) {
        this.applicationKey = applicationKey;
        this.version = version;
        this.value = value;
        this.value2 = value2;
        this.creationTime = creationTime;
        this.expiryTime = expiryTime;
        this.keyState = keyState;
        this.iv = null;
    }

    public void encrypt(Cipher cipher) throws BadPaddingException, IllegalBlockSizeException {
        this.setVersion(Hex.encodeHexString(cipher.doFinal(this.getVersion().getBytes())));

        if (this.value != null)
            this.setValue(Hex.encodeHexString(cipher.doFinal(this.getValue().getBytes())));

        if (this.value2 != null)
            this.setValue2(Hex.encodeHexString(cipher.doFinal(this.getValue2().getBytes())));

        this.setCreationTime(Hex.encodeHexString(cipher.doFinal(this.getCreationTime().getBytes())));

        if (this.expiryTime != null)
            this.setExpiryTime(Hex.encodeHexString(cipher.doFinal(this.getExpiryTime().getBytes())));
    }

    public void decrypt(Cipher cipher) throws BadPaddingException, IllegalBlockSizeException, DecoderException {
        this.setVersion(new String(cipher.doFinal(Hex.decodeHex(this.getVersion()))));

        if (this.value != null)
            this.setValue(new String(cipher.doFinal(Hex.decodeHex(this.getValue()))));

        if (this.value2 != null)
            this.setValue2(new String(cipher.doFinal(Hex.decodeHex(this.getValue2()))));

        this.setCreationTime(new String(cipher.doFinal(Hex.decodeHex(this.getCreationTime()))));

        if (this.expiryTime != null)
            this.setExpiryTime(new String(cipher.doFinal(Hex.decodeHex(this.getExpiryTime()))));
    }

    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    public String getIv() {
        return iv;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ApplicationKey getApplicationKey() {
        return applicationKey;
    }

    public void setApplicationKey(ApplicationKey applicationKey) {
        this.applicationKey = applicationKey;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(String expiryTime) {
        this.expiryTime = expiryTime;
    }

    public KeyState getKeyState() {
        return keyState;
    }

    public void setKeyState(KeyState keyState) {
        this.keyState = keyState;
    }

    @Override
    public String toString() {
        return "Key{" +
            ", version='" + version + '\'' +
            ", value='" + value + '\'' +
            ", value2='" + value2 + '\'' +
            ", creationTime='" + creationTime + '\'' +
            ", expiryTime='" + expiryTime + '\'' +
            ", keyState=" + keyState +
            '}';
    }

    public String generateMac(SecretKey KEK2) {
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(KEK2);
            // creating mac for applicationKey
            byte[] macApplicationKey = mac.doFinal(this.toString().getBytes());

            return Hex.encodeHexString(macApplicationKey);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new InternalServerException("Error generating MAC for Key Version");
        }
    }
}
