package com.chris.softwaresecuritysmodule.entities.dos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.chris.softwaresecuritysmodule.entities.enums.GlobalKeyType;

/**
 * Stores salts for MK1, MK2, KEK1 and KEK2
 */
@Entity
public class GlobalSalt {

    @Id
    @GeneratedValue
    private long id;

    @Column(unique = true)
    private GlobalKeyType keyType;

    @Column(unique = true)
    private String salt;

    public GlobalSalt() {
        // JPA only
    }

    public GlobalSalt(GlobalKeyType keyType, String salt) {
        this.keyType = keyType;
        this.salt = salt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public GlobalKeyType getKeyType() {
        return keyType;
    }

    public void setKeyType(GlobalKeyType keyType) {
        this.keyType = keyType;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}
