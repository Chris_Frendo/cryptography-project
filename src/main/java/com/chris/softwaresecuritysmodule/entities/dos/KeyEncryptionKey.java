package com.chris.softwaresecuritysmodule.entities.dos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class KeyEncryptionKey {

    @Id
    @GeneratedValue
    private long id;

    private String kekValue;
    private String iv;
    private String mac;

    public KeyEncryptionKey() {
        // jpa only
    }

    public KeyEncryptionKey(String kekValue, String iv, String mac) {
        this.kekValue = kekValue;
        this.iv = iv;
        this.mac = mac;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKekValue() {
        return kekValue;
    }

    public void setKekValue(String kekValue) {
        this.kekValue = kekValue;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getIv() {
        return iv;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }
}
