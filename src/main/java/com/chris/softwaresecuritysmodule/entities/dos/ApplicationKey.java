package com.chris.softwaresecuritysmodule.entities.dos;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import com.chris.softwaresecuritysmodule.entities.enums.ApplicationKeyState;
import com.chris.softwaresecuritysmodule.exceptions.InternalServerException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

@Entity
public class ApplicationKey {

    @Id
    @GeneratedValue
    private long id;

    @Column(unique = true)
    private String name;

    @Column(nullable = false)
    private String algorithm;

    @Column(nullable = false)
    private String algorithmParameters;

    @Column()
    private String lifetime;

    @Column(nullable = false)
    private boolean autoRotate;

    @Column(nullable = false)
    private String iv;

    @Column(nullable = false)
    private String mac;

    @Column(nullable = false)
    private ApplicationKeyState applicationKeyState;

    public ApplicationKey() {
        // jpa only
    }

    public ApplicationKey(String name, String algorithm, String algorithmParameters, String lifetime, boolean autoRotate, String iv) {
        this.name = name;
        this.algorithm = algorithm;
        this.algorithmParameters = algorithmParameters;
        this.lifetime = lifetime;
        this.autoRotate = autoRotate;
        this.iv = iv;
        this.applicationKeyState = ApplicationKeyState.ACTIVE;
    }

    public void encrypt(Cipher cipher) throws BadPaddingException, IllegalBlockSizeException {
        this.setAlgorithm(Hex.encodeHexString(cipher.doFinal(this.getAlgorithm().getBytes())));
        this.setAlgorithmParameters(Hex.encodeHexString(cipher.doFinal(this.getAlgorithmParameters().getBytes())));
        if (this.lifetime != null)
            this.setLifetime(Hex.encodeHexString(cipher.doFinal(this.getLifetime().getBytes())));
    }

    public void decrypt(Cipher cipher) throws DecoderException, BadPaddingException, IllegalBlockSizeException {
        this.setAlgorithm(new String(cipher.doFinal(Hex.decodeHex(this.getAlgorithm()))));
        this.setAlgorithmParameters(new String(cipher.doFinal(Hex.decodeHex(this.getAlgorithmParameters()))));
        if (this.lifetime != null)
            this.setLifetime(new String(cipher.doFinal(Hex.decodeHex(this.getLifetime()))));

    }

    public ApplicationKeyState getApplicationKeyState() {
        return applicationKeyState;
    }

    public void setApplicationKeyState(ApplicationKeyState applicationKeyState) {
        this.applicationKeyState = applicationKeyState;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getAlgorithmParameters() {
        return algorithmParameters;
    }

    public void setAlgorithmParameters(String algorithmParameters) {
        this.algorithmParameters = algorithmParameters;
    }

    public String getLifetime() {
        return lifetime;
    }

    public void setLifetime(String lifetime) {
        this.lifetime = lifetime;
    }

    public boolean isAutoRotate() {
        return autoRotate;
    }

    public void setAutoRotate(boolean autoRotate) {
        this.autoRotate = autoRotate;
    }

    public String getIv() {
        return iv;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    @Override
    public String toString() {
        return "ApplicationKey{" +
            "name='" + name + '\'' +
            ", algorithm='" + algorithm + '\'' +
            ", algorithmParameters='" + algorithmParameters + '\'' +
            ", lifetime='" + lifetime + '\'' +
            ", autoRotate=" + autoRotate +
            ", iv='" + iv + '\'' +
            '}';
    }

    public String generateMac(SecretKey KEK2) {
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(KEK2);
            // creating mac for applicationKey
            byte[] macApplicationKey = mac.doFinal(this.toString().getBytes());

            return Hex.encodeHexString(macApplicationKey);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new InternalServerException("Error generating MAC for Application Key");
        }
    }
}
