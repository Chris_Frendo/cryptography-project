package com.chris.softwaresecuritysmodule.entities.enums;

public enum ApplicationKeyState {
    ACTIVE,
    COMPROMISED
}
