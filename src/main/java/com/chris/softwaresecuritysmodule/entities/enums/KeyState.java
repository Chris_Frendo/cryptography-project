package com.chris.softwaresecuritysmodule.entities.enums;

public enum KeyState {
    ACTIVATED,
    DEACTIVATED,
    DESTROYED,
    COMPROMISED
}
