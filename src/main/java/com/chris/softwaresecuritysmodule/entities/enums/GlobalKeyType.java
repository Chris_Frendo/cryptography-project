package com.chris.softwaresecuritysmodule.entities.enums;

public enum GlobalKeyType {
    MK1,
    MK2,
    KEK1,
    KEK2
}
