package com.chris.softwaresecuritysmodule.entities.entitybodies;

public class ChangeMKRequestBody {
    private AdminAuthorization adminAuthorization;
    private AdminAuthorization newAdminParts;

    public ChangeMKRequestBody(AdminAuthorization adminAuthorization, AdminAuthorization newAdminParts) {
        this.adminAuthorization = adminAuthorization;
        this.newAdminParts = newAdminParts;
    }

    public AdminAuthorization getAdminAuthorization() {
        return adminAuthorization;
    }

    public void setAdminAuthorization(AdminAuthorization adminAuthorization) {
        this.adminAuthorization = adminAuthorization;
    }

    public AdminAuthorization getNewAdminParts() {
        return newAdminParts;
    }

    public void setNewAdminParts(AdminAuthorization newAdminParts) {
        this.newAdminParts = newAdminParts;
    }
}
