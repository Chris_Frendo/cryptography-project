package com.chris.softwaresecuritysmodule.entities.entitybodies;

public class EncryptSignRequestResponseBody {

    private String response;
    private String keyVersion;

    public EncryptSignRequestResponseBody(String response, String keyVersion) {
        this.response = response;
        this.keyVersion = keyVersion;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getKeyVersion() {
        return keyVersion;
    }

    public void setKeyVersion(String keyVersion) {
        this.keyVersion = keyVersion;
    }
}
