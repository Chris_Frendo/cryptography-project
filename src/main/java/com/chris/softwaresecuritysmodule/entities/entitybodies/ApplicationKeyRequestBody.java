package com.chris.softwaresecuritysmodule.entities.entitybodies;

public class ApplicationKeyRequestBody {

    private AdminAuthorization adminAuthorization;
    private String name;
    private String algorithm;
    private String algorithmParameters;
    private String lifetime;
    private boolean autoRotate;

    public ApplicationKeyRequestBody(AdminAuthorization adminAuthorization, String name, String algorithm, String algorithmParameters, String lifetime, boolean autoRotate) {
        this.adminAuthorization = adminAuthorization;
        this.name = name;
        this.algorithm = algorithm;
        this.algorithmParameters = algorithmParameters;
        this.lifetime = lifetime;
        this.autoRotate = autoRotate;
    }

    public AdminAuthorization getAdminAuthorization() {
        return adminAuthorization;
    }

    public void setAdminAuthorization(AdminAuthorization adminAuthorization) {
        this.adminAuthorization = adminAuthorization;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getAlgorithmParameters() {
        return algorithmParameters;
    }

    public void setAlgorithmParameters(String algorithmParameters) {
        this.algorithmParameters = algorithmParameters;
    }

    public String getLifetime() {
        return lifetime;
    }

    public void setLifetime(String lifetime) {
        this.lifetime = lifetime;
    }

    public boolean isAutoRotate() {
        return autoRotate;
    }

    public void setAutoRotate(boolean autoRotate) {
        this.autoRotate = autoRotate;
    }
}
