package com.chris.softwaresecuritysmodule.entities.entitybodies;

public class AdminAuthorization {

    private String mkPart1;
    private String mkPart2;

    public AdminAuthorization(String mkPart1, String mkPart2) {
        this.mkPart1 = mkPart1;
        this.mkPart2 = mkPart2;
    }

    public String getWholeKey() {
        if (mkPart1 != null && mkPart2 != null) {
            return mkPart1.concat(mkPart2);
        } else {
            return null;
        }
    }

    public String getMkPart1() {
        return mkPart1;
    }

    public void setMkPart1(String mkPart1) {
        this.mkPart1 = mkPart1;
    }

    public String getMkPart2() {
        return mkPart2;
    }

    public void setMkPart2(String mkPart2) {
        this.mkPart2 = mkPart2;
    }
}
