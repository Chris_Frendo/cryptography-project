package com.chris.softwaresecuritysmodule.entities.entitybodies;

public class VerifyIntegrityRequestBody {

    private String applicationKeyName;
    private String message;
    private String digest;
    private String keyVersion;

    public VerifyIntegrityRequestBody(String applicationKeyName, String message, String digest, String keyVersion) {
        this.applicationKeyName = applicationKeyName;
        this.message = message;
        this.digest = digest;
        this.keyVersion = keyVersion;
    }

    public VerifyIntegrityRequestBody() {
    }

    public String getApplicationKeyName() {
        return applicationKeyName;
    }

    public void setApplicationKeyName(String applicationKeyName) {
        this.applicationKeyName = applicationKeyName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public String getKeyVersion() {
        return keyVersion;
    }

    public void setKeyVersion(String keyVersion) {
        this.keyVersion = keyVersion;
    }
}
