package com.chris.softwaresecuritysmodule.entities.entitybodies;

import java.util.List;

public class ApplicationKeyInfoResponse {

    private ApplicationKeyResponseBody applicationKeyResponseBody;
    private List<KeyVersionResponseBody> keyVersionResponseBodies;

    public ApplicationKeyInfoResponse(ApplicationKeyResponseBody applicationKeyResponseBody, List<KeyVersionResponseBody> keyVersionResponseBodies) {
        this.applicationKeyResponseBody = applicationKeyResponseBody;
        this.keyVersionResponseBodies = keyVersionResponseBodies;
    }

    public ApplicationKeyResponseBody getApplicationKeyResponseBody() {
        return applicationKeyResponseBody;
    }

    public void setApplicationKeyResponseBody(ApplicationKeyResponseBody applicationKeyResponseBody) {
        this.applicationKeyResponseBody = applicationKeyResponseBody;
    }

    public List<KeyVersionResponseBody> getKeyVersionResponseBodies() {
        return keyVersionResponseBodies;
    }

    public void setKeyVersionResponseBodies(List<KeyVersionResponseBody> keyVersionResponseBodies) {
        this.keyVersionResponseBodies = keyVersionResponseBodies;
    }
}
