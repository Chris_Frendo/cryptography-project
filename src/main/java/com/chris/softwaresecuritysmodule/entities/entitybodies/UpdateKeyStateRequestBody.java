package com.chris.softwaresecuritysmodule.entities.entitybodies;

import com.chris.softwaresecuritysmodule.entities.enums.KeyState;

public class UpdateKeyStateRequestBody {

    private AdminAuthorization adminAuthorization;
    private KeyState applicationKeyState;

    public UpdateKeyStateRequestBody(AdminAuthorization adminAuthorization, KeyState applicationKeyState) {
        this.adminAuthorization = adminAuthorization;
        this.applicationKeyState = applicationKeyState;
    }

    public AdminAuthorization getAdminAuthorization() {
        return adminAuthorization;
    }

    public void setAdminAuthorization(AdminAuthorization adminAuthorization) {
        this.adminAuthorization = adminAuthorization;
    }

    public KeyState getApplicationKeyState() {
        return applicationKeyState;
    }

    public ApplicationKeyInfoResponse setNewKeyState(KeyState newKeyState) {
        this.applicationKeyState = newKeyState;

        return null;
    }
}
