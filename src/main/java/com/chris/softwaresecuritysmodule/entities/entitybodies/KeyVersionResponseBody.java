package com.chris.softwaresecuritysmodule.entities.entitybodies;

import com.chris.softwaresecuritysmodule.entities.dos.Key;
import com.chris.softwaresecuritysmodule.entities.enums.KeyState;

public class KeyVersionResponseBody {

    private long id;
    private String version;
    private String creationTime;
    private String expiryTime;
    private KeyState keyState;

    public KeyVersionResponseBody(Key keyVersion) {
        this.id = keyVersion.getId();
        this.version = keyVersion.getVersion();
        this.creationTime = keyVersion.getCreationTime();
        this.expiryTime = keyVersion.getExpiryTime();
        this.keyState = keyVersion.getKeyState();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(String expiryTime) {
        this.expiryTime = expiryTime;
    }

    public KeyState getKeyState() {
        return keyState;
    }

    public void setKeyState(KeyState keyState) {
        this.keyState = keyState;
    }
}
