package com.chris.softwaresecuritysmodule.entities.entitybodies;

import com.chris.softwaresecuritysmodule.entities.dos.ApplicationKey;
import com.chris.softwaresecuritysmodule.entities.enums.ApplicationKeyState;

public class ApplicationKeyResponseBody {

    private long id;
    private String name;
    private String algorithm;
    private String algorithmParameters;
    private String lifetime;
    private boolean autoRotate;
    private ApplicationKeyState applicationKeyState;

    public ApplicationKeyResponseBody(ApplicationKey applicationKey) {
        this.id = applicationKey.getId();
        this.name = applicationKey.getName();
        this.algorithm = applicationKey.getAlgorithm();
        this.algorithmParameters = applicationKey.getAlgorithmParameters();
        this.lifetime = applicationKey.getLifetime();
        this.autoRotate = applicationKey.isAutoRotate();
        this.applicationKeyState = applicationKey.getApplicationKeyState();
    }

    public ApplicationKeyState getApplicationKeyState() {
        return applicationKeyState;
    }

    public void setApplicationKeyState(ApplicationKeyState applicationKeyState) {
        this.applicationKeyState = applicationKeyState;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getAlgorithmParameters() {
        return algorithmParameters;
    }

    public void setAlgorithmParameters(String algorithmParameters) {
        this.algorithmParameters = algorithmParameters;
    }

    public String getLifetime() {
        return lifetime;
    }

    public void setLifetime(String lifetime) {
        this.lifetime = lifetime;
    }

    public boolean isAutoRotate() {
        return autoRotate;
    }

    public void setAutoRotate(boolean autoRotate) {
        this.autoRotate = autoRotate;
    }
}
