package com.chris.softwaresecuritysmodule.entities.entitybodies;

public class EncryptSignRequestBody {

    private String applicationKeyName;
    private String message;

    public EncryptSignRequestBody(String applicationKeyName, String message) {
        this.applicationKeyName = applicationKeyName;
        this.message = message;
    }

    public EncryptSignRequestBody() {
    }

    public String getApplicationKeyName() {
        return applicationKeyName;
    }

    public void setApplicationKeyName(String applicationKeyName) {
        this.applicationKeyName = applicationKeyName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
