package com.chris.softwaresecuritysmodule.entities.entitybodies;

public class DecryptRequestBody {

    private String applicationKeyName;
    private String keyVersion;
    private String cipherText;

    public DecryptRequestBody(String applicationKeyName, String keyVersion, String cipherText) {
        this.applicationKeyName = applicationKeyName;
        this.keyVersion = keyVersion;
        this.cipherText = cipherText;
    }

    public DecryptRequestBody() {
    }

    public String getApplicationKeyName() {
        return applicationKeyName;
    }

    public void setApplicationKeyName(String applicationKeyName) {
        this.applicationKeyName = applicationKeyName;
    }

    public String getKeyVersion() {
        return keyVersion;
    }

    public void setKeyVersion(String keyVersion) {
        this.keyVersion = keyVersion;
    }

    public String getCipherText() {
        return cipherText;
    }

    public void setCipherText(String cipherText) {
        this.cipherText = cipherText;
    }
}
