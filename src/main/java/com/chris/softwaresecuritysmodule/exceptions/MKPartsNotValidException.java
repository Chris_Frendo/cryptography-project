package com.chris.softwaresecuritysmodule.exceptions;

public class MKPartsNotValidException extends RuntimeException {
    public MKPartsNotValidException(String message) {
        super(message);
    }
}
