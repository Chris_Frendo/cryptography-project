package com.chris.softwaresecuritysmodule.exceptions;

public class MacNotVerifiedException extends RuntimeException {

    public MacNotVerifiedException(String message) {
        super(message);
    }
}
