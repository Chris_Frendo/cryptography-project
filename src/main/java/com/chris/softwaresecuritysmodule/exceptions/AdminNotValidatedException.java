package com.chris.softwaresecuritysmodule.exceptions;

public class AdminNotValidatedException extends RuntimeException {

    public AdminNotValidatedException(String message) {
        super(message);
    }
}
