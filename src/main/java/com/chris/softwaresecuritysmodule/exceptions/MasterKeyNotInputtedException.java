package com.chris.softwaresecuritysmodule.exceptions;

public class MasterKeyNotInputtedException extends RuntimeException {

    public MasterKeyNotInputtedException(String message) {
        super(message);
    }
}
