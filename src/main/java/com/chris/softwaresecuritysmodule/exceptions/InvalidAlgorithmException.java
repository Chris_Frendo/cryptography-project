package com.chris.softwaresecuritysmodule.exceptions;

public class InvalidAlgorithmException extends RuntimeException {
    public InvalidAlgorithmException(String message) {
        super(message);
    }
}
