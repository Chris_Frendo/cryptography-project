package com.chris.softwaresecuritysmodule.exceptions;

public class DestroyedKeyException extends RuntimeException {

    public DestroyedKeyException(String message) {
        super(message);
    }
}
