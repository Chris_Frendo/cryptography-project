package com.chris.softwaresecuritysmodule.exceptions;

public class AutoRotateDisabledException extends RuntimeException {

    public AutoRotateDisabledException(String message) {
        super(message);
    }
}
