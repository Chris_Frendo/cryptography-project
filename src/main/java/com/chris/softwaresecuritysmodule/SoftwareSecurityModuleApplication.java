package com.chris.softwaresecuritysmodule;

import com.chris.softwaresecuritysmodule.exceptions.MasterKeyNotInputtedException;
import com.chris.softwaresecuritysmodule.utils.MasterKeyManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@SpringBootApplication
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SoftwareSecurityModuleApplication extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().anyRequest().authenticated()
            .and()
            .csrf().disable()
            .x509()
            .subjectPrincipalRegex("CN=(.*?)(?:,|$)")
            .userDetailsService(userDetailsService());
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return username -> {
            if (username.equals("admin")) {
                return new User(username, "", AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_ADMIN"));
            } else if (username.equals("client")) {
                return new User(username, "", AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_CLIENT"));
            }
            throw new UsernameNotFoundException("User not found!");
        };
    }

    private static Logger LOGGER = LoggerFactory.getLogger(SoftwareSecurityModuleApplication.class);

    public static void main(String[] args) {

        // checking if MK parts are present in args
        if (args.length != 2) {
            LOGGER.error("SSM must be given Master Key parts on startup as 2 arguments");
            throw new MasterKeyNotInputtedException("Master Key parts must be inputted as arguments to SSM");
        }

        // setting master keys in MasterKeyManager
        MasterKeyManager.setKeys(args[0], args[1]);

        LOGGER.info("=============== Starting SSM ===============");
        SpringApplication.run(SoftwareSecurityModuleApplication.class, args);
        LOGGER.info("=============== SSM Started ===============");

        // removing master keys from memory
        MasterKeyManager.destroyKeys();
    }

}
