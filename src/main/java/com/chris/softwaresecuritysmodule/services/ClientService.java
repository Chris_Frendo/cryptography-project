package com.chris.softwaresecuritysmodule.services;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import com.chris.softwaresecuritysmodule.business.KekHelperBO;
import com.chris.softwaresecuritysmodule.entities.dos.ApplicationKey;
import com.chris.softwaresecuritysmodule.entities.dos.Key;
import com.chris.softwaresecuritysmodule.entities.entitybodies.EncryptSignRequestBody;
import com.chris.softwaresecuritysmodule.entities.entitybodies.EncryptSignRequestResponseBody;
import com.chris.softwaresecuritysmodule.entities.enums.GlobalKeyType;
import com.chris.softwaresecuritysmodule.entities.enums.KeyState;
import com.chris.softwaresecuritysmodule.exceptions.AutoRotateDisabledException;
import com.chris.softwaresecuritysmodule.exceptions.DestroyedKeyException;
import com.chris.softwaresecuritysmodule.exceptions.EntityNotFoundException;
import com.chris.softwaresecuritysmodule.exceptions.InternalServerException;
import com.chris.softwaresecuritysmodule.repositories.ApplicationKeyRepository;
import com.chris.softwaresecuritysmodule.repositories.KeyRepository;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.springframework.stereotype.Component;

@Component
public class ClientService {

    private final ApplicationKeyRepository applicationKeyRepository;
    private final KeyRepository keyRepository;
    private final KekHelperBO kekHelperBO;

    public ClientService(ApplicationKeyRepository applicationKeyRepository, KeyRepository keyRepository, KekHelperBO kekHelperBO) {
        this.applicationKeyRepository = applicationKeyRepository;
        this.keyRepository = keyRepository;
        this.kekHelperBO = kekHelperBO;
    }

    private Key handleExpiredKeyVersion(ApplicationKey applicationKey, Cipher decryptionCipher, SecretKey KEK1) {
        if (applicationKey.isAutoRotate()) {

            // getting all key versions to get latest key version
            List<Key> allKeys = keyRepository.findAllByApplicationKey(applicationKey);
            int oldVersion = 0;

            // getting latest version
            oldVersion = kekHelperBO.getOldVersion(decryptionCipher, allKeys, oldVersion);

            // storing version of the new key that is about to be created
            String keyVersionUsed = String.valueOf(oldVersion + 1);

            // generating newKey version
            Key newKey = kekHelperBO.createNewKeyVersion(applicationKey, keyVersionUsed);

            // encrypting newKey
            try {
                // initializing cipher in encryption mode
                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                cipher.init(Cipher.ENCRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(applicationKey.getIv())));
                newKey.encrypt(cipher);

                // generating KEK2 to generate MAC with it
                SecretKey KEK2 = new SecretKeySpec(kekHelperBO.deriveKEKPart(GlobalKeyType.KEK1).getEncoded(), "AES");

                newKey.setMac(newKey.generateMac(KEK2));
            } catch (BadPaddingException | IllegalBlockSizeException | DecoderException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException e) {
                e.printStackTrace();
                throw new InternalServerException("Error encrypting Key Version");
            }

            // saving newKey to vault
            keyRepository.save(newKey);

            // returning newKey: this will be used to encrypt the current message
            return newKey;
        } else {
            throw new AutoRotateDisabledException("There is no active key version and auto rotate is disabled!");
        }
    }

    private Key getLatestActiveVersion(List<Key> keyVersions, Cipher cipher, SecretKey KEK1, SecretKey KEK2, ApplicationKey applicationKey) {
        // active key found, decrypting
        Key activeVersion = keyVersions.get(0);

        try {
            activeVersion.decrypt(cipher);
        } catch (DecoderException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new InternalServerException("Error decrypting Key Version");
        }

        // checking if key has an expiry time
        if (activeVersion.getExpiryTime() != null) {

            // checking if key is expired
            LocalDateTime expiryTime = LocalDateTime.parse(activeVersion.getExpiryTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

            if (expiryTime.isBefore(LocalDateTime.now())) {
                // currently active key is expired

                // re-encrypting activeVersion before calling handleExpiredKeyVersion
                try {
                    Cipher eCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                    eCipher.init(Cipher.ENCRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(applicationKey.getIv())));

                    // updating state of active key to deactivated
                    activeVersion.setKeyState(KeyState.DEACTIVATED);
                    activeVersion.encrypt(eCipher);
                    applicationKey.encrypt(eCipher);

                    activeVersion.setMac(activeVersion.generateMac(KEK2));

                    // updating vault with new key state
                    keyRepository.save(activeVersion);
                } catch (BadPaddingException | IllegalBlockSizeException | DecoderException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException e) {
                    e.printStackTrace();
                    throw new InternalServerException("Error encrypting Key Version");
                }

                // key version is expired so new key version must be created if application is set to auto_rotate
                activeVersion = handleExpiredKeyVersion(applicationKey, cipher, KEK1);

                // decrypting the activeVersion to be used for encrypting the message
                try {
                    activeVersion.decrypt(cipher);
                } catch (DecoderException | BadPaddingException | IllegalBlockSizeException e) {
                    e.printStackTrace();
                    throw new InternalServerException("Error decrypting Key Version");
                }
            }
        }

        return activeVersion;
    }

    public EncryptSignRequestResponseBody encryptSignMessageHandler(EncryptSignRequestBody encryptSignRequestBody, boolean isEncrypt) {
        // finding requested application key
        ApplicationKey applicationKey = applicationKeyRepository.findByName(encryptSignRequestBody.getApplicationKeyName())
            .orElseThrow(() -> new EntityNotFoundException("Application Key with name " + encryptSignRequestBody.getApplicationKeyName() + " was not found"));

        // goes over all key values and application key and perform integrity check using KEK2
        kekHelperBO.integrityCheckApplicationKeyAndKeyValues(applicationKey);

        // getting KEK1 and KEK2
        SecretKey KEK1;
        SecretKey KEK2;
        try {
            KEK2 = new SecretKeySpec(kekHelperBO.deriveKEKPart(GlobalKeyType.KEK2).getEncoded(), "AES");
            KEK1 = new SecretKeySpec(kekHelperBO.deriveKEKPart(GlobalKeyType.KEK1).getEncoded(), "AES");
        } catch (DecoderException e) {
            e.printStackTrace();
            throw new InternalServerException("Error creating key spec for KEK1");
        }

        // creating cipher to decrypt application key and latest key version stored in vault
        Cipher cipher;
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(applicationKey.getIv())));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | DecoderException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            throw new InternalServerException("Error creating cipher to decrypt vault contents");
        }

        // getting key version of application key
        List<Key> keyVersions = keyRepository.findAllByApplicationKeyAndKeyState(applicationKey, KeyState.ACTIVATED);

        // decrypting application key
        try {
            applicationKey.decrypt(cipher);
        } catch (DecoderException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new InternalServerException("Error decrypting Application Key");
        }

        boolean isAsymmetric = false;

        if (applicationKey.getAlgorithm().contains("RSA")) {
            // asymmetric key
            isAsymmetric = true;
        }

        Key activeVersion;

        // getting latest active version if it exists
        if (keyVersions.size() != 0) {
            activeVersion = getLatestActiveVersion(keyVersions, cipher, KEK1, KEK2, applicationKey);
        } else {
            // active key NOT found

            // handling expired key scenario: generates a new key version if applicationKey marked as auto_rotate
            activeVersion = handleExpiredKeyVersion(applicationKey, cipher, KEK1);

            // decrypting the activeVersion to be used for encrypting the message
            try {
                activeVersion.decrypt(cipher);
            } catch (DecoderException | BadPaddingException | IllegalBlockSizeException e) {
                e.printStackTrace();
                throw new InternalServerException("Error decrypting Key Version");
            }
        }

        if (isEncrypt) {
            return encryptMessage(encryptSignRequestBody, activeVersion, applicationKey, KEK1, KEK2, isAsymmetric);
        } else {
            return signMessage(encryptSignRequestBody, activeVersion, applicationKey);
        }
    }

    private EncryptSignRequestResponseBody encryptMessage(EncryptSignRequestBody encryptSignRequestBody, Key activeVersion, ApplicationKey applicationKey, SecretKey KEK1, SecretKey KEK2, boolean asymmetric) {

        String keyVersionUsed = activeVersion.getVersion();

        // creating Secret Key from current key version value
        SecretKey appSecretKey;
        try {
            appSecretKey = new SecretKeySpec(Hex.decodeHex(activeVersion.getValue()), applicationKey.getAlgorithm());
        } catch (DecoderException e) {
            e.printStackTrace();
            throw new InternalServerException("Error creating key spec from Application Key stored in vault");
        }

        String encryptedMessage;
        try {
            // creating cipher to encrypt message using algorithm of application key encoded in Base64
            Cipher cipher = Cipher.getInstance(applicationKey.getAlgorithm().concat(applicationKey.getAlgorithmParameters()));

            if (activeVersion.getIv() != null) {
                cipher.init(Cipher.ENCRYPT_MODE, appSecretKey, new IvParameterSpec(Hex.decodeHex(activeVersion.getIv())));
            } else {
                // this must be first time encrypting a message so IV needs to be generated and saved in vault with key

                if (asymmetric) {
                    PublicKey publicKey = getPublicKey(applicationKey.getAlgorithm(), activeVersion);
                    cipher.init(Cipher.ENCRYPT_MODE, publicKey);
                } else {
                    cipher.init(Cipher.ENCRYPT_MODE, appSecretKey);
                }

                // checking if an IV was randomly generated (for example if using CBC mode then IV is randomly generated. With ECB it is not generated
                if (cipher.getIV() != null) {
                    activeVersion.setIv(Hex.encodeHexString(cipher.getIV()));

                    // re encrypting active version and saving to DB with new IV
                    Cipher eCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                    eCipher.init(Cipher.ENCRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(applicationKey.getIv())));
                    applicationKey.encrypt(eCipher);
                    activeVersion.encrypt(eCipher);

                    activeVersion.setMac(activeVersion.generateMac(KEK2));

                    keyRepository.save(activeVersion);
                }

            }

            encryptedMessage = Base64.encodeBase64String(cipher.doFinal(encryptSignRequestBody.getMessage().getBytes()));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | DecoderException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
            throw new InternalServerException("Error encrypting message using application key");
        }

        return new EncryptSignRequestResponseBody(encryptedMessage, keyVersionUsed);
    }

    private PrivateKey getPrivateKey(String algorithm, Key activeVersion) {
        // creating private key
        PrivateKey privateKey;
        try {
            KeyFactory kf = KeyFactory.getInstance(algorithm);
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(Hex.decodeHex(activeVersion.getValue2()));
            privateKey = kf.generatePrivate(privateKeySpec);
        } catch (DecoderException | InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new InternalServerException("Error creating private key spec from Application Key stored in vault");
        }

        return privateKey;
    }

    private PublicKey getPublicKey(String algorithm, Key activeVersion) {
        // creating public key
        PublicKey publicKey;
        try {
            KeyFactory kf = KeyFactory.getInstance(algorithm);
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(Hex.decodeHex(activeVersion.getValue()));
            publicKey = kf.generatePublic(publicKeySpec);
        } catch (DecoderException | InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new InternalServerException("Error creating public key spec from Application Key stored in vault");
        }

        return publicKey;
    }

    private EncryptSignRequestResponseBody signMessage(EncryptSignRequestBody signRequestBody, Key activeVersion, ApplicationKey applicationKey) {

        String keyVersionUsed = activeVersion.getVersion();

        // creating private key
        PrivateKey privateKey = getPrivateKey(applicationKey.getAlgorithm(), activeVersion);

        String signatureHex;
        try {
            Signature privateSignature = Signature.getInstance("SHA256withRSA");
            privateSignature.initSign(privateKey);
            privateSignature.update(signRequestBody.getMessage().getBytes());
            signatureHex = Hex.encodeHexString(privateSignature.sign());
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            e.printStackTrace();
            throw new InternalServerException("Error Signing message");
        }

        return new EncryptSignRequestResponseBody(signatureHex, keyVersionUsed);
    }

    public String decryptVerifyHandler(String applicationKeyName, String keyVersion, String cipherText, String digest, boolean isDecrypt) {
        // finding requested application key
        ApplicationKey applicationKey = applicationKeyRepository.findByName(applicationKeyName)
            .orElseThrow(() -> new EntityNotFoundException("Application Key with name " + applicationKeyName + " was not found"));

        // goes over all key values and application key and perform integrity check using KEK2
        kekHelperBO.integrityCheckApplicationKeyAndKeyValues(applicationKey);

        // getting key version of application key and key version
        List<Key> keyVersions = keyRepository.findAllByApplicationKey(applicationKey);

        // getting KEK1
        SecretKey KEK1;
        try {
            KEK1 = new SecretKeySpec(kekHelperBO.deriveKEKPart(GlobalKeyType.KEK1).getEncoded(), "AES");
        } catch (DecoderException e) {
            e.printStackTrace();
            throw new InternalServerException("Error creating key spec for KEK1");
        }

        // creating cipher to decrypt application key and latest key version stored in vault
        Cipher cipher;
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(applicationKey.getIv())));
            applicationKey.decrypt(cipher);

            // decrypting
            for (Key k : keyVersions) {
                k.decrypt(cipher);
            }
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | DecoderException | InvalidAlgorithmParameterException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new InternalServerException("Error decrypting vault contents");
        }

        boolean isAsymmetric = false;

        if (applicationKey.getAlgorithm().contains("RSA")) {
            isAsymmetric = true;
        }

        // filtering key versions to get the requested key version for decryption
        keyVersions = keyVersions.stream().filter(key -> key.getVersion().equals(keyVersion)).collect(Collectors.toList());

        if (keyVersions.size() != 1) {
            throw new InternalServerException("Required Key Version Not Found");
        }

        Key requiredKeyVersion = keyVersions.get(0);

        if (requiredKeyVersion.getKeyState().equals(KeyState.DESTROYED)) {
            throw new DestroyedKeyException("Key version " + keyVersion + " has been destroyed");
        }

        if (isDecrypt) {
            return decryptMessage(applicationKey, requiredKeyVersion, cipherText, isAsymmetric);
        } else {
            return verifyMessageIntegrity(applicationKey, requiredKeyVersion, cipherText, digest);
        }
    }

    private String decryptMessage(ApplicationKey applicationKey, Key requiredKeyVersion, String cipherText, boolean asymmetric) {

        // creating Secret Key from current key version value
        SecretKey appSecretKey;
        try {
            appSecretKey = new SecretKeySpec(Hex.decodeHex(requiredKeyVersion.getValue()), applicationKey.getAlgorithm());
        } catch (DecoderException e) {
            e.printStackTrace();
            throw new InternalServerException("Error creating key spec from Application Key stored in vault");
        }

        String decryptedMessage;
        try {
            // creating cipher to decrypt message using algorithm of application key encoded in Base64
            Cipher cipher = Cipher.getInstance(applicationKey.getAlgorithm().concat(applicationKey.getAlgorithmParameters()));

            if (requiredKeyVersion.getIv() != null) {
                cipher.init(Cipher.DECRYPT_MODE, appSecretKey, new IvParameterSpec(Hex.decodeHex(requiredKeyVersion.getIv())));
            } else {
                // we are encrypting without an IV because the algorithm does not need one

                if (asymmetric) {
                    PrivateKey privateKey = getPrivateKey(applicationKey.getAlgorithm(), requiredKeyVersion);
                    cipher.init(Cipher.DECRYPT_MODE, privateKey);
                } else {
                    cipher.init(Cipher.DECRYPT_MODE, appSecretKey);
                }

            }

            decryptedMessage = new String(cipher.doFinal(Base64.decodeBase64(cipherText)));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | DecoderException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
            throw new InternalServerException("Error decrypting message using application key");
        }

        return decryptedMessage;
    }

    private String verifyMessageIntegrity(ApplicationKey applicationKey, Key requiredKeyVersion, String message, String digest) {

        // creating public key
        PublicKey publicKey = getPublicKey(applicationKey.getAlgorithm(), requiredKeyVersion);

        boolean result;
        try {
            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initVerify(publicKey);
            signature.update(message.getBytes());
            result = signature.verify(Hex.decodeHex(digest));
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException | DecoderException e) {
            e.printStackTrace();
            throw new InternalServerException("Error Verifying message");
        }

        return String.valueOf(result);
    }
}
