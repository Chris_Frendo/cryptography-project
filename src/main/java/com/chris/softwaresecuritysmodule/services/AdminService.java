package com.chris.softwaresecuritysmodule.services;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.stream.Collectors;

import com.chris.softwaresecuritysmodule.business.KekHelperBO;
import com.chris.softwaresecuritysmodule.entities.dos.ApplicationKey;
import com.chris.softwaresecuritysmodule.entities.dos.GlobalSalt;
import com.chris.softwaresecuritysmodule.entities.dos.Key;
import com.chris.softwaresecuritysmodule.entities.dos.KeyEncryptionKey;
import com.chris.softwaresecuritysmodule.entities.entitybodies.AdminAuthorization;
import com.chris.softwaresecuritysmodule.entities.entitybodies.ApplicationKeyInfoResponse;
import com.chris.softwaresecuritysmodule.entities.entitybodies.ApplicationKeyRequestBody;
import com.chris.softwaresecuritysmodule.entities.entitybodies.ApplicationKeyResponseBody;
import com.chris.softwaresecuritysmodule.entities.entitybodies.ChangeMKRequestBody;
import com.chris.softwaresecuritysmodule.entities.entitybodies.KeyVersionResponseBody;
import com.chris.softwaresecuritysmodule.entities.entitybodies.UpdateKeyStateRequestBody;
import com.chris.softwaresecuritysmodule.entities.enums.ApplicationKeyState;
import com.chris.softwaresecuritysmodule.entities.enums.GlobalKeyType;
import com.chris.softwaresecuritysmodule.entities.enums.KeyState;
import com.chris.softwaresecuritysmodule.exceptions.AdminNotValidatedException;
import com.chris.softwaresecuritysmodule.exceptions.EntityNotFoundException;
import com.chris.softwaresecuritysmodule.exceptions.InternalServerException;
import com.chris.softwaresecuritysmodule.exceptions.InvalidAlgorithmException;
import com.chris.softwaresecuritysmodule.exceptions.MKPartsNotValidException;
import com.chris.softwaresecuritysmodule.repositories.ApplicationKeyRepository;
import com.chris.softwaresecuritysmodule.repositories.GlobalSaltRepository;
import com.chris.softwaresecuritysmodule.repositories.KeyEncryptionKeyRepository;
import com.chris.softwaresecuritysmodule.repositories.KeyRepository;
import com.chris.softwaresecuritysmodule.utils.KeyEncryptionKeyManager;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AdminService {

    private static Logger LOGGER = LoggerFactory.getLogger(AdminService.class);
    private final ApplicationKeyRepository applicationKeyRepository;
    private final KeyRepository keyRepository;
    private final KekHelperBO kekHelperBO;
    private final GlobalSaltRepository globalSaltRepository;
    private final KeyEncryptionKeyRepository keyEncryptionKeyRepository;

    private List<ApplicationKey> currentApplicationKeyList;
    private List<Key> currentKeys;

    public AdminService(KekHelperBO kekHelperBO, ApplicationKeyRepository applicationKeyRepository, KeyRepository keyRepository, GlobalSaltRepository globalSaltRepository, KeyEncryptionKeyRepository keyEncryptionKeyRepository) {
        this.kekHelperBO = kekHelperBO;
        this.applicationKeyRepository = applicationKeyRepository;
        this.keyRepository = keyRepository;
        this.globalSaltRepository = globalSaltRepository;
        this.keyEncryptionKeyRepository = keyEncryptionKeyRepository;
    }

    private void validateAdminHelper(String wholeMK) {
        String decryptedKek;
        try {
            // validating admin credentials
            decryptedKek = kekHelperBO.validateAdmin(wholeMK);
        } catch (NoSuchAlgorithmException | InvalidKeyException | DecoderException | InvalidKeySpecException | IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException | NoSuchPaddingException e) {
            throw new AdminNotValidatedException("Error validating admin");
        }

        // match decryptedKek with stored Kek for validation for good measure
        if (!decryptedKek.equals(KeyEncryptionKeyManager.getKek())) {
            throw new AdminNotValidatedException("Decrypted KEK does not match with what is stored in memory");
        }
    }

    public ApplicationKeyResponseBody createApplicationKey(ApplicationKeyRequestBody applicationKeyRequestBody) {
        // validating admin credentials
        validateAdminHelper(applicationKeyRequestBody.getAdminAuthorization().getWholeKey());

        // validating algorithm parameters
        try {
            Cipher.getInstance(applicationKeyRequestBody.getAlgorithm().concat(applicationKeyRequestBody.getAlgorithmParameters()));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            LOGGER.error("Invalid algorithm or algorithm parameters passed", e);
            throw new InvalidAlgorithmException("Invalid algorithm or parameters passed");
        }

        // deriving KEK1 and creating Cipher instance
        SecretKey KEK1 = deriveKEK1Helper();
        Cipher cipher = createAESCBCCipherHelper();

        // initializing cipher in encrypt mode with KEK1 as the key
        try {
            cipher.init(Cipher.ENCRYPT_MODE, KEK1);
        } catch (InvalidKeyException e) {
            throw new InternalServerException("Invalid KEK used to init Cipher");
            // should never reach here
        }

        // creating new application key
        ApplicationKey newApplicationKey = new ApplicationKey(applicationKeyRequestBody.getName(), applicationKeyRequestBody.getAlgorithm(),
            applicationKeyRequestBody.getAlgorithmParameters(), applicationKeyRequestBody.getLifetime(), applicationKeyRequestBody.isAutoRotate(),
            Hex.encodeHexString(cipher.getIV()));

        // creating new key version and linking to application key
        Key newKey = kekHelperBO.createNewKeyVersion(newApplicationKey, "1");

        ApplicationKeyResponseBody response = new ApplicationKeyResponseBody(newApplicationKey);

        try {
            // encrypting data structure fields
            newKey.encrypt(cipher);
            newApplicationKey.encrypt(cipher);
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            throw new InternalServerException("Error encrypting application key");
        }

        // deriving KEK2
        SecretKey KEK2 = deriveKEK2Helper();

        // creating MAC of newly generated key and storing it in ApplicationKey data structure for Strong Integrity Check using KEK2
        String applicationKeyMac = newApplicationKey.generateMac(KEK2);
        newApplicationKey.setMac(applicationKeyMac);

        // creating MAC of new key version and storing it in Key data structure
        String keyVersionMac = newKey.generateMac(KEK2);
        newKey.setMac(keyVersionMac);

        // saving new application key and key version to DB
        applicationKeyRepository.save(newApplicationKey);
        keyRepository.save(newKey);

        response.setId(newApplicationKey.getId());

        return response;
    }

    /**
     * Helper function used to derive KEK2
     *
     * @return SecretKey holding KEK2
     */
    private SecretKey deriveKEK2Helper() {
        try {
            // deriving KEK2 from KEK and creating key from it
            return new SecretKeySpec(kekHelperBO.deriveKEKPart(GlobalKeyType.KEK2).getEncoded(), "AES");
        } catch (DecoderException e) {
            LOGGER.error("Error deriving KEK2", e);
            throw new InternalServerException("Error deriving KEK2");
        }
    }

    /**
     * Helper function used to derive KEK1
     *
     * @return SecretKey holding KEK1
     */
    private SecretKey deriveKEK1Helper() {
        // deriving KEK part
        try {
            return new SecretKeySpec(kekHelperBO.deriveKEKPart(GlobalKeyType.KEK1).getEncoded(), "AES");
        } catch (DecoderException e) {
            LOGGER.error("Error deriving KEK1", e);
            throw new InternalServerException("Error deriving KEK1");
        }
    }

    /**
     * Helper function used to create a Cipher in AES/CBC/PKCS5Padding mode
     *
     * @return a Cipher instance of mode AES/CBC/PKCS5Padding
     */
    private Cipher createAESCBCCipherHelper() {
        // creating cipher
        try {
            return Cipher.getInstance("AES/CBC/PKCS5Padding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            LOGGER.error("Error when creating decryption cipher", e);
            throw new InternalServerException("Error when decrypting application keys");
        }
    }

    /**
     * Service method used to get summary on all application keys
     *
     * @param adminAuthorization Contains admin MK parts for authorization
     * @return List of ApplicationKeyResponseBody which contain summary info on all application keys in the vault
     */
    public List<ApplicationKeyResponseBody> getAllApplicationKeys(AdminAuthorization adminAuthorization) {
        // validating admin key parts
        validateAdminHelper(adminAuthorization.getWholeKey());

        // deriving KEK1 and creating Cipher instance
        final SecretKey KEK1 = deriveKEK1Helper();
        Cipher cipher = createAESCBCCipherHelper();

        return applicationKeyRepository.findAll().stream().map(applicationKey -> {

            // goes over all key values and application key and perform integrity check using KEK2
            kekHelperBO.integrityCheckApplicationKeyAndKeyValues(applicationKey);

            try {
                cipher.init(Cipher.DECRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(applicationKey.getIv())));
                applicationKey.decrypt(cipher);
            } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException | DecoderException | InvalidAlgorithmParameterException e) {
                LOGGER.error("Error creating decryption cipher", e);
                throw new InternalServerException("Error creating decryption cipher");
            }

            return new ApplicationKeyResponseBody(applicationKey);
        }).collect(Collectors.toList());
    }

    /**
     * Service method used to get detailed information about a specific application key stored in the vault
     *
     * @param id                 The id of the application key requested
     * @param adminAuthorization Contains admin MK parts for authorization
     * @return Object containing information about the Application Key and all of its versions (mac, iv and key values are not returned)
     */
    public ApplicationKeyInfoResponse getApplicationKeyById(long id, AdminAuthorization adminAuthorization) {
        // validating admin key parts
        validateAdminHelper(adminAuthorization.getWholeKey());

        // deriving KEK1 and KEK2
        final SecretKey KEK1 = deriveKEK1Helper();

        // Creating Cipher instance
        Cipher cipher = createAESCBCCipherHelper();

        // getting application key from repository
        ApplicationKey applicationKey = applicationKeyRepository.findById(id)
            .orElseThrow(() -> new EntityNotFoundException("Application Key with id " + id + " was not found"));

        // goes over all key values and application key and perform integrity check using KEK2
        kekHelperBO.integrityCheckApplicationKeyAndKeyValues(applicationKey);

        // getting all key versions
        List<Key> keyVersions = keyRepository.findAllByApplicationKey(applicationKey);

        // getting key versions for application key from repo and mapping to key version response body
        List<KeyVersionResponseBody> keyVersionResponseBodies = keyVersions.stream().map(keyVersion -> {
            try {
                // decryption key version fields
                cipher.init(Cipher.DECRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(applicationKey.getIv())));
                keyVersion.decrypt(cipher);
            } catch (BadPaddingException | IllegalBlockSizeException | DecoderException | InvalidAlgorithmParameterException | InvalidKeyException e) {
                LOGGER.error("Error decrypting key version {}. Key Version might be compromised: ", keyVersion.getVersion(), e);
            }

            return new KeyVersionResponseBody(keyVersion);
        }).collect(Collectors.toList());

        try {
            // decrypting application key fields
            cipher.init(Cipher.DECRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(applicationKey.getIv())));
            applicationKey.decrypt(cipher);
        } catch (BadPaddingException | IllegalBlockSizeException | DecoderException | InvalidAlgorithmParameterException | InvalidKeyException e) {
            LOGGER.error("Error decrypting Application Key: ", e);
            throw new InternalServerException("Error decrypting Application Key");
        }

        // creating new ApplicationKeyInfoResponse from decrypted fields to return back to user
        return new ApplicationKeyInfoResponse(new ApplicationKeyResponseBody(applicationKey), keyVersionResponseBodies);
    }

    public ApplicationKeyResponseBody updateApplicationKey(long id, ApplicationKeyRequestBody applicationKeyRequestBody) {
        // validating admin key parts
        validateAdminHelper(applicationKeyRequestBody.getAdminAuthorization().getWholeKey());

        // validating algorithm parameters
        try {
            Cipher.getInstance(applicationKeyRequestBody.getAlgorithm().concat(applicationKeyRequestBody.getAlgorithmParameters()));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            LOGGER.error("Invalid algorithm or algorithm parameters passed", e);
            throw new InvalidAlgorithmException("Invalid algorithm or parameters passed");
        }

        // getting application key from DB
        ApplicationKey applicationKey = applicationKeyRepository.findById(id)
            .orElseThrow(() -> new EntityNotFoundException("Application Key with id " + id + " was not found"));

        // goes over all key values and application key and perform integrity check using KEK2
        kekHelperBO.integrityCheckApplicationKeyAndKeyValues(applicationKey);

        // updating applicationKey fields
        applicationKey.setLifetime(applicationKeyRequestBody.getLifetime());
        applicationKey.setName(applicationKeyRequestBody.getName());
        applicationKey.setAlgorithm(applicationKeyRequestBody.getAlgorithm());
        applicationKey.setAlgorithmParameters(applicationKeyRequestBody.getAlgorithmParameters());
        applicationKey.setAutoRotate(applicationKeyRequestBody.isAutoRotate());
        applicationKey.setApplicationKeyState(ApplicationKeyState.ACTIVE);

        // deriving KEK and creating new cipher
        SecretKey KEK1 = deriveKEK1Helper();
        SecretKey KEK2 = deriveKEK2Helper();
        Cipher cipher = createAESCBCCipherHelper();

        // getting previous keyValues to get version to create new version value
        List<Key> keyVersions = keyRepository.findAllByApplicationKey(applicationKey);
        int oldVersion = 0;

        try {
            cipher.init(Cipher.DECRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(applicationKey.getIv())));
            for (Key k : keyVersions) {
                String version = new String(cipher.doFinal(Hex.decodeHex(k.getVersion())));
                if (Integer.parseInt(version) > oldVersion) {
                    if (k.getKeyState().equals(KeyState.ACTIVATED)) {
                        // deactivating old key and saving key with new state to repo
                        k.setKeyState(KeyState.DEACTIVATED);

                        // generating new mac for old key version
                        k.setMac(k.generateMac(KEK2));

                        // saving updated key version to repo
                        keyRepository.save(k);
                    }

                    oldVersion = Integer.parseInt(version);
                }
            }
        } catch (InvalidKeyException | InvalidAlgorithmParameterException | DecoderException | BadPaddingException | IllegalBlockSizeException e) {
            LOGGER.error("Error decrypting key version", e);
            throw new InternalServerException("Error decrypting Key version");
        }

        // generating new key version object
        Key newKeyVersion = kekHelperBO.createNewKeyVersion(applicationKey, String.valueOf(oldVersion + 1));

        ApplicationKeyResponseBody response = new ApplicationKeyResponseBody(applicationKey);

        try {
            // initializing cipher in encrypt mode with KEK1 as the key and encrypting new application key values
            cipher.init(Cipher.ENCRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(applicationKey.getIv())));

            applicationKey.encrypt(cipher);
            newKeyVersion.encrypt(cipher);

            // generating new macs for updated values
            applicationKey.setMac(applicationKey.generateMac(KEK2));
            newKeyVersion.setMac(newKeyVersion.generateMac(KEK2));
        } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException | DecoderException | InvalidAlgorithmParameterException e) {
            throw new InternalServerException("Invalid KEK used to init Cipher");
            // should never reach here
        }

        // saving new application key and key version to DB
        applicationKeyRepository.save(applicationKey);
        keyRepository.save(newKeyVersion);

        return response;
    }

    public ApplicationKeyInfoResponse updateApplicationKeyState(long id, UpdateKeyStateRequestBody updateKeyStateRequestBody) {
        // validating admin key parts
        validateAdminHelper(updateKeyStateRequestBody.getAdminAuthorization().getWholeKey());

        // getting application key from DB
        ApplicationKey applicationKey = applicationKeyRepository.findById(id)
            .orElseThrow(() -> new EntityNotFoundException("Application Key with id " + id + " was not found"));

        // goes over all key values and application key and perform integrity check using KEK2
        kekHelperBO.integrityCheckApplicationKeyAndKeyValues(applicationKey);

        if (!updateKeyStateRequestBody.getApplicationKeyState().equals(KeyState.ACTIVATED)) {
            // getting key versions of application key
            List<Key> keyVersions = keyRepository.findAllByApplicationKey(applicationKey);

            List<Key> activeKeys = keyVersions.stream().filter(key -> key.getKeyState().equals(KeyState.ACTIVATED)).collect(Collectors.toList());

            SecretKey KEK1 = deriveKEK1Helper();
            SecretKey KEK2 = deriveKEK2Helper();
            Cipher cipher = this.createAESCBCCipherHelper();

            String previousVersion;

            if (activeKeys.size() == 1) {
                // there is an active version so we mark it as the admin requested. else just create the new key version

                Key activeVersion = activeKeys.get(0);

                // updating state of currently active version and storing encrypted object back in db
                try {
                    cipher.init(Cipher.DECRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(applicationKey.getIv())));
                    activeVersion.decrypt(cipher);

                    activeVersion.setKeyState(updateKeyStateRequestBody.getApplicationKeyState());
                    previousVersion = activeVersion.getVersion();

                    // setting value to null if admin wants to set key state to destroyed
                    if (updateKeyStateRequestBody.getApplicationKeyState().equals(KeyState.DESTROYED)) {
                        activeVersion.setValue(null);
                        activeVersion.setValue2(null);
                    }

                    cipher.init(Cipher.ENCRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(applicationKey.getIv())));
                    activeVersion.encrypt(cipher);

                    // generating new mac for old key version
                    activeVersion.setMac(activeVersion.generateMac(KEK2));

                    keyRepository.save(activeVersion);
                } catch (InvalidKeyException | InvalidAlgorithmParameterException | DecoderException | BadPaddingException | IllegalBlockSizeException e) {
                    e.printStackTrace();
                    return null;
                }

            } else {
                try {
                    cipher.init(Cipher.DECRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(applicationKey.getIv())));
                } catch (InvalidKeyException | InvalidAlgorithmParameterException | DecoderException e) {
                    e.printStackTrace();
                    return null;
                }
                int oldVersion = 0;
                // getting latest version
                oldVersion = kekHelperBO.getOldVersion(cipher, keyVersions, oldVersion);

                previousVersion = String.valueOf(oldVersion);
            }

            // decrypting application key to use parameters such as algorithm
            try {
                cipher.init(Cipher.DECRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(applicationKey.getIv())));
                applicationKey.decrypt(cipher);
            } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException | DecoderException e) {
                e.printStackTrace();
                return null;
            }

            // generating new key version object
            Key newKeyVersion = kekHelperBO.createNewKeyVersion(applicationKey, String.valueOf(Integer.parseInt(previousVersion) + 1));

            try {
                // initializing cipher in encrypt mode with KEK1 as the key and encrypting new application key values
                cipher.init(Cipher.ENCRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(applicationKey.getIv())));

                applicationKey.encrypt(cipher);
                newKeyVersion.encrypt(cipher);

                // generating new macs for updated values
                newKeyVersion.setMac(newKeyVersion.generateMac(KEK2));
            } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException | DecoderException | InvalidAlgorithmParameterException e) {
                throw new InternalServerException("Invalid KEK used to init Cipher");
                // should never reach here
            }

            // saving new application key and key version to DB
            applicationKeyRepository.save(applicationKey);
            keyRepository.save(newKeyVersion);

        }
        return null;
    }

    private void readEntireVault() throws BadPaddingException, IllegalBlockSizeException, DecoderException, InvalidAlgorithmParameterException, InvalidKeyException {
        // getting all Application Keys
        this.currentApplicationKeyList = applicationKeyRepository.findAll();

        // getting all Key Versions
        this.currentKeys = keyRepository.findAll();

        // getting KEK1 for decrypting vault
        SecretKey KEK1 = deriveKEK1Helper();

        // decrypting all application keys
        Cipher decryptionCipher = createAESCBCCipherHelper();

        // decrypting all application keys
        for (ApplicationKey ak : this.currentApplicationKeyList) {
            kekHelperBO.integrityCheckApplicationKeyAndKeyValues(ak);

            decryptionCipher.init(Cipher.DECRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(ak.getIv())));
            ak.decrypt(decryptionCipher);
        }
        // decrypting all key versions
        for (Key k : this.currentKeys) {
            decryptionCipher.init(Cipher.DECRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(k.getApplicationKey().getIv())));
            k.decrypt(decryptionCipher);
        }
    }

    private void generateNewKEKSalts() {
        // generating new salts for KEK1 and KEK2
        SecureRandom secureRandom = new SecureRandom();

        byte[] saltKEK1 = new byte[32];
        byte[] saltKEK2 = new byte[32];
        secureRandom.nextBytes(saltKEK1);
        secureRandom.nextBytes(saltKEK2);

        GlobalSalt globalSaltKEK1 = globalSaltRepository.findByKeyType(GlobalKeyType.KEK1)
            .orElseThrow(() -> new EntityNotFoundException("Salt for KEK1 not found"));

        GlobalSalt globalSaltKEK2 = globalSaltRepository.findByKeyType(GlobalKeyType.KEK2)
            .orElseThrow(() -> new EntityNotFoundException("Salt for KEK2 not found"));

        globalSaltKEK1.setSalt(Hex.encodeHexString(saltKEK1));
        globalSaltKEK2.setSalt(Hex.encodeHexString(saltKEK2));

        // saving new kek1 and kek2 salts to db
        globalSaltRepository.save(globalSaltKEK1);
        globalSaltRepository.save(globalSaltKEK2);
    }

    private void encryptAndSaveVault() throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException, DecoderException, InvalidAlgorithmParameterException {
        // deriving new KEK1 and KEK2
        SecretKey KEK1 = deriveKEK1Helper();
        SecretKey KEK2 = deriveKEK2Helper();

        Cipher encryptionCipher = createAESCBCCipherHelper();

        // encrypting each Application key using the newly created KEK
        for (ApplicationKey ak : this.currentApplicationKeyList) {
            encryptionCipher.init(Cipher.ENCRYPT_MODE, KEK1);
            ak.encrypt(encryptionCipher);
            ak.setIv(Hex.encodeHexString(encryptionCipher.getIV()));
            ak.setMac(ak.generateMac(KEK2));

            applicationKeyRepository.save(ak);
        }

        // encrypting each Key value using the newly created KEK
        for (Key k : this.currentKeys) {
            encryptionCipher.init(Cipher.ENCRYPT_MODE, KEK1, new IvParameterSpec(Hex.decodeHex(k.getApplicationKey().getIv())));
            k.encrypt(encryptionCipher);
            k.setMac(k.generateMac(KEK2));

            keyRepository.save(k);
        }
    }

    public void rotateKeyEncryptionKey(AdminAuthorization adminAuthorization) throws DecoderException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeySpecException {
        // validating admin key parts
        validateAdminHelper(adminAuthorization.getWholeKey());

        // reading and decrypting entire vault contents
        readEntireVault();

        byte[] saltMK1 = Hex.decodeHex(globalSaltRepository.findByKeyType(GlobalKeyType.MK1)
            .orElseThrow(() -> new EntityNotFoundException("Salt for MK1 not found")).getSalt());

        byte[] saltMK2 = Hex.decodeHex(globalSaltRepository.findByKeyType(GlobalKeyType.MK2)
            .orElseThrow(() -> new EntityNotFoundException("Salt for MK2 not found")).getSalt());

        // creating new KeyEncryptionKey (this stores the un encrypted new kek value in memory)
        KeyEncryptionKey newKEK = kekHelperBO.generateNewKEK(adminAuthorization.getWholeKey(), saltMK1, saltMK2);

        // getting existing KEK record from repo
        KeyEncryptionKey existingKekRecord = keyEncryptionKeyRepository.findAll().get(0);

        // updating fields of existing kek record with new kek fields
        existingKekRecord.setKekValue(newKEK.getKekValue());
        existingKekRecord.setIv(newKEK.getIv());
        existingKekRecord.setMac(newKEK.getMac());

        keyEncryptionKeyRepository.save(existingKekRecord);

        // generating new salts for KEK1 and KEK2
        generateNewKEKSalts();

        // encrypting vault using new KEK and saving
        encryptAndSaveVault();
    }

    public void changeMasterKey(ChangeMKRequestBody changeMKRequestBody) throws BadPaddingException, IllegalBlockSizeException, DecoderException, InvalidAlgorithmParameterException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeySpecException {
        // validating admin key parts
        validateAdminHelper(changeMKRequestBody.getAdminAuthorization().getWholeKey());

        // validating new mk parts
        if (!kekHelperBO.validateMKParts(changeMKRequestBody.getNewAdminParts().getMkPart1(), changeMKRequestBody.getNewAdminParts().getMkPart2())) {
            throw new MKPartsNotValidException("MK parts do not adhere to password rules.\n" +
                "MK parts must have a combination of upper and lower case letters, numbers and special characters, " +
                "there must be no spaces and at least 16 chars long");
        }

        // reading and decrypting vault contents
        readEntireVault();

        // getting new MK
        String newMK = changeMKRequestBody.getNewAdminParts().getWholeKey();

        // generating new 32 bit salts for MK1 and MK2
        SecureRandom secureRandom = new SecureRandom();
        byte[] saltMK1 = new byte[32];
        byte[] saltMK2 = new byte[32];
        secureRandom.nextBytes(saltMK1);
        secureRandom.nextBytes(saltMK2);

        // getting MK1 salt from vault
        GlobalSalt adminSaltMK1 = globalSaltRepository.findByKeyType(GlobalKeyType.MK1)
            .orElseThrow(() -> new EntityNotFoundException("Admin Salt for MK1 not found"));

        // getting MK2 salt from vault
        GlobalSalt adminSaltMK2 = globalSaltRepository.findByKeyType(GlobalKeyType.MK2)
            .orElseThrow(() -> new EntityNotFoundException("Admin Salt for MK2 not found"));

        // updating salts in vault
        adminSaltMK1.setSalt(Hex.encodeHexString(saltMK1));
        adminSaltMK2.setSalt(Hex.encodeHexString(saltMK2));

        // saving MK1 and MK2 salts to DB for later use
        globalSaltRepository.save(adminSaltMK1);
        globalSaltRepository.save(adminSaltMK2);

        // getting a new KEK record from the new MK
        KeyEncryptionKey newKEK = kekHelperBO.generateNewKEK(newMK, saltMK1, saltMK2);

        // getting current kek record from vault
        KeyEncryptionKey existingKekRecord = keyEncryptionKeyRepository.findAll().get(0);

        existingKekRecord.setKekValue(newKEK.getKekValue());
        existingKekRecord.setIv(newKEK.getIv());
        existingKekRecord.setMac(newKEK.getMac());

        // save new Kek record to DB
        keyEncryptionKeyRepository.save(existingKekRecord);

        // generating and saving new kek salts
        generateNewKEKSalts();

        // re encrypting vault contents with new KEK and saving
        encryptAndSaveVault();
    }
}
