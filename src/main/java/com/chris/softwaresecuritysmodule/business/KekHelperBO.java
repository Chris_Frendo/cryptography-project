package com.chris.softwaresecuritysmodule.business;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.chris.softwaresecuritysmodule.entities.dos.ApplicationKey;
import com.chris.softwaresecuritysmodule.entities.dos.KeyEncryptionKey;
import com.chris.softwaresecuritysmodule.entities.enums.ApplicationKeyState;
import com.chris.softwaresecuritysmodule.entities.enums.GlobalKeyType;
import com.chris.softwaresecuritysmodule.entities.enums.KeyState;
import com.chris.softwaresecuritysmodule.exceptions.EntityNotFoundException;
import com.chris.softwaresecuritysmodule.exceptions.InternalServerException;
import com.chris.softwaresecuritysmodule.exceptions.InvalidAlgorithmException;
import com.chris.softwaresecuritysmodule.exceptions.MacNotVerifiedException;
import com.chris.softwaresecuritysmodule.repositories.ApplicationKeyRepository;
import com.chris.softwaresecuritysmodule.repositories.GlobalSaltRepository;
import com.chris.softwaresecuritysmodule.repositories.KeyEncryptionKeyRepository;
import com.chris.softwaresecuritysmodule.repositories.KeyRepository;
import com.chris.softwaresecuritysmodule.utils.KeyEncryptionKeyManager;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class KekHelperBO {

    private static Logger LOGGER = LoggerFactory.getLogger(KekHelperBO.class);

    private final KeyEncryptionKeyRepository keyEncryptionKeyRepository;
    private final GlobalSaltRepository globalSaltRepository;
    private final KeyRepository keyRepository;
    private final ApplicationKeyRepository applicationKeyRepository;

    private final static String PASS_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{16,}$";


    public KekHelperBO(KeyEncryptionKeyRepository keyEncryptionKeyRepository, GlobalSaltRepository globalSaltRepository, KeyRepository keyRepository, ApplicationKeyRepository applicationKeyRepository) {
        this.keyEncryptionKeyRepository = keyEncryptionKeyRepository;
        this.globalSaltRepository = globalSaltRepository;
        this.keyRepository = keyRepository;
        this.applicationKeyRepository = applicationKeyRepository;
    }

    public boolean validateMKParts(String mk1, String mk2) {
        return mk1.matches(PASS_REGEX) && mk2.matches(PASS_REGEX);
    }

    /**
     * Function used to decrypt the KEK retrieved from the DB using the provided MK1
     *
     * @param mk1 The first part of the MK which was used to encrypt the KEK, will be used to decrypt the KEK now
     * @return hex string of the decrypted KEK
     */
    private String decryptKek(byte[] mk1) throws DecoderException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        // get first record from Key table
        KeyEncryptionKey kek = keyEncryptionKeyRepository.findAll().get(0);

        // Creating KeySpec from MK1
        Key mk1KeySpec = new SecretKeySpec(mk1, "AES");

        // creating IvParameterSpec from iv bytes stored in admin salt
        IvParameterSpec ivSpec = new IvParameterSpec(Hex.decodeHex(kek.getIv()));

        // creating new cipher for algorithm and parameters stored in kek
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

        // initializing cipher in decrypt mode with the MK mk1KeySpec and specifying the iv
        cipher.init(Cipher.DECRYPT_MODE, mk1KeySpec, ivSpec);

        // decrypting kek using the cipher and MK mk1KeySpec
        byte[] decryptedKek = cipher.doFinal(Hex.decodeHex(kek.getKekValue()));

        // getting hex string from decryptedKek
        return Hex.encodeHexString(decryptedKek);
    }

    /**
     * Helper function used to validate mk2 by creating and validating the MAC of the encoded KEK
     *
     * @param mk2 The inputted MK part that was used to created the MAC of the encrypted KEK
     * @return true if MAC verified, else false
     */
    private boolean verifyMacKek(byte[] mk2) throws NoSuchAlgorithmException, InvalidKeyException, DecoderException {
        KeyEncryptionKey kek = keyEncryptionKeyRepository.findAll().get(0);

        // Creating KeySpec from MK2
        Key mk2KeySpec = new SecretKeySpec(mk2, "AES");

        // creating HMAC instance
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(mk2KeySpec);

        // macing kek using mk2KeySpec
        byte[] macEncodedKEK = mac.doFinal(Hex.decodeHex(kek.getKekValue()));
        String macEncodedKEKHex = Hex.encodeHexString(macEncodedKEK);

        return macEncodedKEKHex.equals(kek.getMac());
    }

    public String validateAdmin(String mk) throws NoSuchAlgorithmException, InvalidKeyException, DecoderException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchPaddingException {
        // getting MK1 salt from vault
        byte[] saltMK1 = Hex.decodeHex(globalSaltRepository.findByKeyType(GlobalKeyType.MK1)
            .orElseThrow(() -> new EntityNotFoundException("Admin Salt for MK1 not found")).getSalt());

        // getting MK2 salt from vault
        byte[] saltMK2 = Hex.decodeHex(globalSaltRepository.findByKeyType(GlobalKeyType.MK2)
            .orElseThrow(() -> new EntityNotFoundException("Admin Salt for MK2 not found")).getSalt());

        // generating MK1
        SecretKeyFactory kf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec specs = new PBEKeySpec(mk.toCharArray(), saltMK1, 10000, 256);
        SecretKey MK1 = kf.generateSecret(specs);

        // generating MK2
        specs = new PBEKeySpec(mk.toCharArray(), saltMK2, 10000, 512);
        SecretKey MK2 = kf.generateSecret(specs);

        // Verifying generated MAC with the one stored in DB
        if (this.verifyMacKek(MK2.getEncoded())) {
            LOGGER.info("MAC of encrypted KEK verified successfully");
        } else {
            LOGGER.error("MAC of encrypted KEK NOT verified. Shutting down");
            throw new MacNotVerifiedException("MAC of encrypted KEK not verified");
        }

        // decrypting KEK and returning result
        return this.decryptKek(MK1.getEncoded());
    }

    private SecretKey generateKekPart(byte[] salt) {

        SecretKeyFactory kf;
        try {
            kf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        } catch (NoSuchAlgorithmException e) {
            throw new InternalServerException("Error creating hmac instance");
        }

        KeySpec specs = new PBEKeySpec(KeyEncryptionKeyManager.getKek().toCharArray(), salt, 10000, 256);

        SecretKey kekPart;
        try {
            kekPart = kf.generateSecret(specs);
        } catch (InvalidKeySpecException e) {
            throw new InternalServerException("Error deriving kekPart from KEK");
        }

        return kekPart;
    }

    public SecretKey deriveKEKPart(GlobalKeyType kekType) throws DecoderException {
        if (!(kekType.equals(GlobalKeyType.KEK1) || kekType.equals(GlobalKeyType.KEK2))) {
            return null;
        }

        byte[] saltKEK = Hex.decodeHex(globalSaltRepository.findByKeyType(kekType)
            .orElseThrow(() -> new EntityNotFoundException("Salt not found")).getSalt());

        return generateKekPart(saltKEK);
    }

    public void integrityCheckApplicationKeyAndKeyValues(ApplicationKey applicationKey) {
        final SecretKey KEK2;
        try {
            KEK2 = deriveKEKPart(GlobalKeyType.KEK2);
        } catch (DecoderException e) {
            LOGGER.error("Error deriving KEK2", e);
            throw new InternalServerException("Error deriving KEK2");
        }

        // computing mac of ApplicationKey
        String mac = applicationKey.generateMac(KEK2);

        // validating mac with what is stored in applicationKey
        if (!mac.equals(applicationKey.getMac()) && applicationKey.getApplicationKeyState().equals(ApplicationKeyState.ACTIVE)) {
            // setting application key to compromised if it is currently ACTIVE and the mac did not match
            applicationKey.setApplicationKeyState(ApplicationKeyState.COMPROMISED);
            applicationKeyRepository.save(applicationKey);
        }

        List<com.chris.softwaresecuritysmodule.entities.dos.Key> keyVersions = keyRepository.findAllByApplicationKey(applicationKey);

        // computing mac of individual keyVersions
        keyVersions.forEach(key -> {
            String keyVersionMac = key.generateMac(KEK2);

            // validating mac with what is stored in the key
            if (!keyVersionMac.equals(key.getMac())) {
                if (key.getKeyState().equals(KeyState.ACTIVATED) || key.getKeyState().equals(KeyState.DEACTIVATED)) {
                    // setting key state to compromised
                    key.setKeyState(KeyState.COMPROMISED);

                    // saving key with COMPROMISED state to vault
                    keyRepository.save(key);
                }
            }
        });

    }

    private String generateNewApplicationKeyValue(String algorithm) {
        // generating a new application key generator with the requested algorithm
        KeyGenerator keyGen;
        try {
            keyGen = KeyGenerator.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new InvalidAlgorithmException("Invalid Algorithm passed");
        }

        SecureRandom secRandom = new SecureRandom();
        keyGen.init(secRandom);

        // generating a new application key value
        return Hex.encodeHexString(keyGen.generateKey().getEncoded());
    }

    public com.chris.softwaresecuritysmodule.entities.dos.Key createNewKeyVersion(ApplicationKey applicationKey, String version) {

        String hexValue;
        String privateHexValue = null;

        // handling case where new key is asymmetric key
        if (applicationKey.getAlgorithm().contains("RSA")) {
            SecureRandom secureRandom = new SecureRandom();

            KeyPairGenerator keyPairGenerator;

            try {
                keyPairGenerator = KeyPairGenerator.getInstance(applicationKey.getAlgorithm());
            } catch (NoSuchAlgorithmException e) {
                LOGGER.error(e.getMessage());
                throw new InvalidAlgorithmException("Error creating key pair with algorithm: " + applicationKey.getAlgorithm());
            }

            keyPairGenerator.initialize(2048, secureRandom);

            KeyPair keyPair = keyPairGenerator.generateKeyPair();

            hexValue = Hex.encodeHexString(keyPair.getPublic().getEncoded());
            privateHexValue = Hex.encodeHexString(keyPair.getPrivate().getEncoded());
        } else {
            // generating new application key value
            hexValue = generateNewApplicationKeyValue(applicationKey.getAlgorithm());
        }

        // creating new creation and expiration dates
        LocalDateTime localDateTime = LocalDateTime.now();
        String currentDate = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String expireDate = null;

        if (applicationKey.getLifetime() != null) {
            localDateTime = localDateTime.plusDays(Integer.parseInt(applicationKey.getLifetime()));
            expireDate = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }

        return new com.chris.softwaresecuritysmodule.entities.dos.Key(applicationKey, version, hexValue, privateHexValue, currentDate, expireDate, KeyState.ACTIVATED);
    }

    public int getOldVersion(Cipher decryptionCipher, List<com.chris.softwaresecuritysmodule.entities.dos.Key> allKeys, int oldVersion) {
        for (com.chris.softwaresecuritysmodule.entities.dos.Key k : allKeys) {
            String version;

            try {
                version = new String(decryptionCipher.doFinal(Hex.decodeHex(k.getVersion())));
            } catch (IllegalBlockSizeException | BadPaddingException | DecoderException e) {
                e.printStackTrace();
                throw new InternalServerException("Error decoding key version");
            }

            if (Integer.parseInt(version) > oldVersion) {
                oldVersion = Integer.parseInt(version);
            }
        }
        return oldVersion;
    }

    public KeyEncryptionKey generateNewKEK(String mk, byte[] saltMK1, byte[] saltMK2) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        // generating MK1
        SecretKeyFactory kf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec specs = new PBEKeySpec(mk.toCharArray(), saltMK1, 10000, 256);
        SecretKey MK1 = kf.generateSecret(specs);

        // generating MK2
        specs = new PBEKeySpec(mk.toCharArray(), saltMK2, 10000, 512);
        SecretKey MK2 = kf.generateSecret(specs);

        // generating new KEK
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        SecureRandom secRandom = new SecureRandom();

        keyGen.init(secRandom);
        SecretKey newKEK = keyGen.generateKey();

        // Creating KeySpec from MK1
        Key mk1KeySpec = new SecretKeySpec(MK1.getEncoded(), "AES");

        // creating new cipher for AES with CBC and PKCS5Padding.
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

        // initializing cipher in encrypt mode with the MK mk1KeySpec and specifying the initialization vector
        cipher.init(Cipher.ENCRYPT_MODE, mk1KeySpec);

        // storing un encrypted kek value in memory
        KeyEncryptionKeyManager.setKek(Hex.encodeHexString(newKEK.getEncoded()));

        // encrypting kek
        byte[] kekValue = cipher.doFinal(newKEK.getEncoded());

        // Creating KeySpec from MK2
        Key mk2KeySpec = new SecretKeySpec(MK2.getEncoded(), "AES");

        // creating HMAC instance
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(mk2KeySpec);

        // macing kekValue using mk2KeySpec
        byte[] macEncodedKEK = mac.doFinal(kekValue);

        // created kek record
        return new KeyEncryptionKey(Hex.encodeHexString(kekValue), Hex.encodeHexString(cipher.getIV()), Hex.encodeHexString(macEncodedKEK));
    }
}