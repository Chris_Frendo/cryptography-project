package com.chris.softwaresecuritysmodule.repositories;

import com.chris.softwaresecuritysmodule.entities.dos.KeyEncryptionKey;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KeyEncryptionKeyRepository extends JpaRepository<KeyEncryptionKey, Long> {
}
