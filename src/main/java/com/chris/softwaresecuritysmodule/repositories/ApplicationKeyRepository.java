package com.chris.softwaresecuritysmodule.repositories;

import java.util.Optional;

import com.chris.softwaresecuritysmodule.entities.dos.ApplicationKey;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationKeyRepository extends JpaRepository<ApplicationKey, Long> {

    Optional<ApplicationKey> findByName(String name);
}
