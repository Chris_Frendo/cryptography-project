package com.chris.softwaresecuritysmodule.repositories;

import java.util.List;

import com.chris.softwaresecuritysmodule.entities.dos.ApplicationKey;
import com.chris.softwaresecuritysmodule.entities.dos.Key;
import com.chris.softwaresecuritysmodule.entities.enums.KeyState;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KeyRepository extends JpaRepository<Key, Long> {

    List<Key> findAllByApplicationKey(ApplicationKey applicationKey);

    List<Key> findAllByApplicationKeyAndKeyState(ApplicationKey applicationKey, KeyState keyState);
}
