package com.chris.softwaresecuritysmodule.repositories;

import java.util.Optional;

import com.chris.softwaresecuritysmodule.entities.dos.GlobalSalt;
import com.chris.softwaresecuritysmodule.entities.enums.GlobalKeyType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GlobalSaltRepository extends JpaRepository<GlobalSalt, Long> {

    Optional<GlobalSalt> findByKeyType(GlobalKeyType globalKeyType);
}
