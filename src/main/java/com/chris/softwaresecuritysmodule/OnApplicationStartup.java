package com.chris.softwaresecuritysmodule;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import com.chris.softwaresecuritysmodule.business.KekHelperBO;
import com.chris.softwaresecuritysmodule.entities.dos.GlobalSalt;
import com.chris.softwaresecuritysmodule.entities.dos.KeyEncryptionKey;
import com.chris.softwaresecuritysmodule.entities.enums.GlobalKeyType;
import com.chris.softwaresecuritysmodule.exceptions.MKPartsNotValidException;
import com.chris.softwaresecuritysmodule.repositories.GlobalSaltRepository;
import com.chris.softwaresecuritysmodule.repositories.KeyEncryptionKeyRepository;
import com.chris.softwaresecuritysmodule.utils.KeyEncryptionKeyManager;
import com.chris.softwaresecuritysmodule.utils.MasterKeyManager;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class OnApplicationStartup {

    private static Logger LOGGER = LoggerFactory.getLogger(OnApplicationStartup.class);

    private final KeyEncryptionKeyRepository keyEncryptionKeyRepository;
    private final GlobalSaltRepository globalSaltRepository;

    private final KekHelperBO kekHelperBO;

    public OnApplicationStartup(KeyEncryptionKeyRepository keyEncryptionKeyRepository, GlobalSaltRepository globalSaltRepository, KekHelperBO kekHelperBO) {
        this.keyEncryptionKeyRepository = keyEncryptionKeyRepository;
        this.globalSaltRepository = globalSaltRepository;
        this.kekHelperBO = kekHelperBO;
    }

    @PostConstruct
    public void onApplicationStartupEvent() throws NoSuchPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException, DecoderException, InvalidKeySpecException {
        LOGGER.info("Checking if KEK is already stored in Vault");

        // checking that inputted MK parts are valid
        if (!kekHelperBO.validateMKParts(MasterKeyManager.getPartA(), MasterKeyManager.getPartB())) {
            throw new MKPartsNotValidException("MK parts do not adhere to password rules.\n" +
                "MK parts must have a combination of upper and lower case letters, numbers and special characters, " +
                "there must be no spaces and at least 16 chars long");
        }

        // concatenating MK parts
        String mk = MasterKeyManager.getPartA().concat(MasterKeyManager.getPartB());

        // removing original MK parts from memory
        MasterKeyManager.destroyKeys();

        // checking if kek exists already
        boolean kekNotFound = keyEncryptionKeyRepository.findAll().isEmpty();

        if (kekNotFound) {
            // calling function to create KEK and MACed KEK and store in DB
            createNewKEK(mk);
        } else {
            // KEK found in DB, so validating MAC
            KeyEncryptionKeyManager.setKek(kekHelperBO.validateAdmin(mk));
        }
    }

    /**
     * Helper function used to create KEK and MACed KEK and stores them in DB
     */
    private void createNewKEK(String mk) throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException, InvalidKeySpecException {
        LOGGER.info("Started process of creating and encoding KEK and MACed KEK");

        // generating 32 byte salts for MK1 and MK2
        SecureRandom secureRandom = new SecureRandom();
        byte[] saltMK1 = new byte[32];
        byte[] saltMK2 = new byte[32];
        secureRandom.nextBytes(saltMK1);
        secureRandom.nextBytes(saltMK2);

        // getting a new KEK record
        KeyEncryptionKey keyEncryptionKey = kekHelperBO.generateNewKEK(mk, saltMK1, saltMK2);

        // save Kek record to DB
        keyEncryptionKeyRepository.save(keyEncryptionKey);

        // creating records for salts
        GlobalSalt adminSaltMK1 = new GlobalSalt(GlobalKeyType.MK1, Hex.encodeHexString(saltMK1));
        GlobalSalt adminSaltMK2 = new GlobalSalt(GlobalKeyType.MK2, Hex.encodeHexString(saltMK2));

        // saving MK1 and MK2 salts to DB for later use
        globalSaltRepository.save(adminSaltMK1);
        globalSaltRepository.save(adminSaltMK2);

        LOGGER.info("Successfully saved new KEK and MACed KEK to Vault");

        // generating and storing salts for KEK1 and KEK2
        byte[] saltKEK1 = new byte[32];
        byte[] saltKEK2 = new byte[32];
        secureRandom.nextBytes(saltKEK1);
        secureRandom.nextBytes(saltKEK2);

        GlobalSalt kek1Salt = new GlobalSalt(GlobalKeyType.KEK1, Hex.encodeHexString(saltKEK1));
        GlobalSalt kek2Salt = new GlobalSalt(GlobalKeyType.KEK2, Hex.encodeHexString(saltKEK2));

        // saving kek1 and kek2 salts to db
        globalSaltRepository.save(kek1Salt);
        globalSaltRepository.save(kek2Salt);
    }

}

